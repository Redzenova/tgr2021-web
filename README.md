## :computer: TESA TOPGUN 2021 - [Server]


## DAY1 - 22/02/2021
:triangular_flag_on_post: **ความต้องการ**
```
 - [X] สามารถเข้าใช้งาน Cloud Server ได้
 - [X] มีการใช้งาน Git Repository ในการเก็บข้อมูล Source Code
 - [X] ติดตั้ง Web Server ให้สามารถใช้งานได้
	 - [X] ใช้งาน Web Server ได้
	 - [X] ใช้งาน PHP ได้
	 - [X] ใช้งาน MariaDB ได้
	 - [X] ใช้งาน phpMyAdmin ได้
 - [X] ติดตั้ง Composer Package Manager และเรียกใช้งานคำสั่งได้
 - [X] ติดตั้งและสามารถเรียกใช้งาน Laravel ได้
 - [X] มีการสร้างและใช้งาน Laravel Migrations
 - [ ] มีการสร้างและใช้งาน Laravel Database Seeding
		 (แต่ละทีมต้องนำชื่อ และ นามสกุล ของสมาชิกทุกคนในทีม เป็นข้อมูลในการทำ seeding แล้วก็ต้อง commit ไปที่ git ด้วย)
 - [ ] มีการออกแบบและสร้าง API ที่มีการเรียกข้อมูลจากฐานข้อมูลไว้ใช้งาน
		1.ข้อมูลมาจากฐานข้อมูล
		2.ข้อมูลเป็น JSON
 - [ ] มีการตรวจสอบการใช้งาน API ด้วย JWT (Authentication by JWT)
		 (แสดงให้เห็นผ่านทาง POSTMAN)
```
<br>

**Basic Command for RHEL 8 / CentOS8**
```
--- อัปเดต rpm / package
# yum update

--- ตั้งค่าระบบเครือข่าย หรือ interface
# nmtui

--- ดูรายละเอียดระบบเครือข่าย
# ip addr
```
**ติดตั้ง PHP**
```
--- ติดตั้ง repository เพิ่มเติมสำหรับการใช้งาน
# dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
# dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
# yum install composer wget git -y

--- เปิดใช้งาน PHP
# dnf module enable php:remi-7.4
# yum update
# yum install php php-cli php-json php-mbstring php-pdo php-pecl-zip php-mysqlnd
```
**ติดตั้ง Web Server**
```
--- ติดตั้ง Apache Web Service
# yum install httpd
# systemctl start httpd
# systemctl enable httpd

--- Allow port in Firewall
# firewall-cmd --zone=public  --add-service=http
# firewall-cmd --zone=public  --add-service=https

ทดลองเรียกใช้งานผ่าน browser จาก http://[ไอพีที่ได้รับมา]
```
**ติดตั้ง Database Server**
```
--- ติดตั้ง MariaDB
# yum install mariadb-server
# systemctl start mariadb
# systemctl enable mariadb

--- ทดสอบการติดตั้ง mariadb
# mysql
# exit()

--- เปลี่ยนรหัสผ่าน mysql
# mysql_secure_installation
--- ทำการตั้งรหัสผ่านและทดลองใช้งาน
# mysql -p root -p
--- สร้างฐานข้อมูล
# mysql -u root -p -e "CREATE DATABASE IF NOT EXISTS tgr2021 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;"
```

**ติดตั้ง phpmyadmin**
```
# cd /var/www/html
# wget [https://files.phpmyadmin.net/phpMyAdmin/4.9.4/phpMyAdmin-4.9.4-all-languages.zip](https://files.phpmyadmin.net/phpMyAdmin/4.9.4/phpMyAdmin-4.9.4-all-languages.zip)
# unzip phpMyAdmin-4.9.4-all-languages.zip
# mv phpMyAdmin-4.9.4-all-languages phpmyadmin
# chown -R apache:apache /var/www/html/phpmyadmin
# cd /var/www/html/phpmyadmin
# mv config.sample.inc.php config.inc.php
# nano config.inc.php
	แก้ไขบรรทัด
	$cfg['blowfish_secret'] = 'รหัสที่ต้องการ';

# mysql < sql/create_tables.sql -u root -p
# sed -i '14 s/Require local/Require all granted/' /etc/httpd/conf.d/phpMyAdmin.conf
```
**สร้าง  Laravel Project**
```
# cd /var/www/html export COMPOSER_ALLOW_SUPERUSER=1; composer create-project laravel/laravel tgr2021 
# sed -i '122 s/\/var\/www\/html/\/var\/www\/html\/tgr2021\/public/' /etc/httpd/conf/httpd.conf
# sed -i '154 s/AllowOverride None/AllowOverride All/' /etc/httpd/conf/httpd.conf 
# chown -R apache.apache tgr2021/ 
# chmod -R 755 tgr2021 
# chmod -R 755 tgr2021/storage chcon -R -t httpd_sys_rw_content_t tgr2021/storage 
# setsebool httpd_can_network_connect_db 1 
# systemctl start httpd
```
**สร้าง   Repository** 
```
# git config --global user.email "your email" 
# git config --global user.name "your username" 
# git config --global credential.helper store 
# cd tgr2021 
# git init 
# git add . 
# git commit -m "init" 
# git remote add origin your-remote-url.git 
# git push -u origin master
```